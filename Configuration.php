<?php
/**
 * Created by PhpStorm.
 * User: lidaltc
 * Date: 15/11/2016
 * Time: 7:52 PM
 */

//define("DB_HOST","mysql.comp.polyu.edu.hk");
//define("DB_USER","16020601d");
//define("DB_PASSWORD","ppzzwfig");
//define("DATABASE_NAME","16020601d");
//define("PROJECT_DIR",dirname(__FILE__));
//define("AUTOIMPORT",PROJECT_DIR."/autoimport");

define("DB_HOST","localhost");
define("DB_USER","ubuntu");
define("DB_PASSWORD","1234567890");
define("DATABASE_NAME","webindi");
define("PROJECT_DIR",dirname(__FILE__));
define("AUTOIMPORT",PROJECT_DIR."/autoimport");

$session_id = session_id();
if(sizeof($session_id)<=0)
    session_start();

global $auto_imported;

//auto import
function importRecursive($dir_path){
    global $auto_imported;
    $auto_import = scandir($dir_path);
    foreach($auto_import as $child){
        if(in_array($child,array(".",".."))) continue;
        if(is_dir($dir_path."/".$child)){
            importRecursive($dir_path."/".$child);
        }
        if(!preg_match("/.*\.php$/", $child)) continue;
        require_once($dir_path."/".$child);
        $auto_imported[] = str_replace(".php","",$child);
    }
}
importRecursive(AUTOIMPORT);

if(class_exists('DatabaseAccess')){
    global $dbAccess;
    $dbAccess = new DatabaseAccess();
    $dbAccess->query(
        "CREATE TABLE IF NOT EXISTS `reserve_record` (
            `ID` INT NOT NULL AUTO_INCREMENT,
          `rStore` INT NOT NULL,
          `model` VARCHAR(20) NOT NULL,
          `fname` VARCHAR(20) NOT NULL,
          `lname` VARCHAR(20) NOT NULL,
          `remail` VARCHAR(100) NOT NULL,
          `phone` VARCHAR(10) NOT NULL,
          `create_time` TIMESTAMP(6) NOT NULL,
          `status` VARCHAR(10) NULL,
         PRIMARY KEY (`ID`))"
    );
}
?>