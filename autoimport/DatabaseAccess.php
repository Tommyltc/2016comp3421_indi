<?php

/**
 * Created by PhpStorm.
 * User: lindaltc
 * Date: 16/11/2016
 * Time: 12:51 AM
 */
class DatabaseAccess
{
    const ARRAY_A = 0;
    const OBJECT_A = 1;

    /**
     * @var DatabaseAccess
     */
    public static $databaseAccess;

    /**
     * @var mysqli
     */
    public $sqlconn;

    /**
     * @return DatabaseAccess
     */
    public static function getInstance()
    {
        if (!isset(DatabaseAccess::$databaseAccess)) {
            DatabaseAccess::$databaseAccess = new DatabaseAccess();
        }
        return DatabaseAccess::$databaseAccess;
    }

    /**
     * DatabaseAccess constructor.
     */
    public function __construct()
    {
        $this->sqlconn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysqli_connect_errno()) {
            die(mysqli_connect_error());
        } else {
            $result = $this->sqlconn->query("SELECT table_name, column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '".DATABASE_NAME."' ORDER BY table_name, ordinal_position");
            if($result->num_rows<=0) {
                $create_result = $this->sqlconn->query("CREATE SCHEMA " . DATABASE_NAME);
                LogUtil::savelog("Schema \"".DATABASE_NAME."\" auto created");
            }
            mysqli_select_db($this->sqlconn, DATABASE_NAME);
            $this->sqlconn->autocommit(true);
        }
    }

    /**
     * @param $statement string
     * @param $type integer
     * @return array
     */
    public function query($statement, $type = 0)
    {
        LogUtil::savelog($statement);
        $dbresults = $this->sqlconn->query($statement);
        LogUtil::savelog(json_encode($dbresults));
        if($this->sqlconn->insert_id!=0){
            return array("insert_id"=>$this->sqlconn->insert_id);
        }
        if(sizeof($this->sqlconn->error)>0){
            LogUtil::savelog($this->sqlconn->error);
        }
        if($dbresults===1||$dbresults===true){
            LogUtil::savelog("update success");
            return array("update_row_count"=>$this->sqlconn->affected_rows);
        }
        $result = array();
        switch ($type) {
            case DatabaseAccess::ARRAY_A:
                while ($dbresult = $dbresults->fetch_array(MYSQLI_ASSOC))
                    $result[] = $dbresult;
                break;
            case DatabaseAccess::OBJECT_A:
                while ($dbresult = $dbresults->fetch_object())
                    $result[] = $dbresult;
                braek;
        }
        $dbresults->close();
        return $result;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->sqlconn->error;
    }

    public function closeDB()
    {
        $this->sqlconn->close();
    }

    /**
     * @param string $tableName
     * @return bool
     */
    public function tableExist($tableName){
        $result = $this->sqlconn->query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = \"".DATABASE_NAME."\" AND TABLE_NAME = \"".$tableName."\"");
        return $result->num_rows>0;
    }
}

?>