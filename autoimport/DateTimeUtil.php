<?php

/**
 * Created by PhpStorm.
 * User: lindaltc
 * Date: 28/11/2016
 * Time: 1:33 AM
 */
class DateTimeUtil
{
    public static function getDateStart(DateTime $date){
        $date->setTime(0,0,0);
        return $date;
    }
    public static function getDateEnd(DateTime $date){
        $date->setTime(23,59,59);
        return $date;
    }
}