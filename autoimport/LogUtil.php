<?php

/**
 * Created by PhpStorm.
 * User: lindaltc
 * Date: 16/11/2016
 * Time: 2:07 AM
 */
class LogUtil
{
    public static $printLogIndicator = true;
    public static $saveLogIndicator = true;
    private static $FolderName = "LogMonitor";
    private static $LogName = "LogMonitor";

    public static function printLog($value){
        if(!LogUtil::$printLogIndicator){
            return ;
        }
        if(is_array($value)||is_object($value)){
            $printString = json_encode($value);
        }else{
            $printString = $value;
        }
        echo $printString;
    }

    public static function savelog($value){
        if(!LogUtil::$saveLogIndicator){
            return ;
        }
        try{
            if(!file_exists(PROJECT_DIR.'/'.LogUtil::$FolderName.'/')){
                mkdir(PROJECT_DIR.'/'.LogUtil::$FolderName.'/',0775,true);
                $newtext = fopen(PROJECT_DIR.'/'.LogUtil::$FolderName.'/'.LogUtil::$LogName.'.log', 'w');
                fclose($newtext);
            }else{
                $file = file(PROJECT_DIR.'/'.LogUtil::$FolderName.'/'.LogUtil::$LogName.'.log');
                $countLine = count($file);
                if($countLine>=10000){
                    if(file_exists(PROJECT_DIR.'/'.LogUtil::$FolderName.'/'.LogUtil::$LogName.'.log')){
                        rename(PROJECT_DIR.'/'.LogUtil::$FolderName.'/'.LogUtil::$LogName.'.log', PROJECT_DIR.'/'.LogUtil::$FolderName.'/LM_'.date('Ymd_His',time()).'.log');
                        $newtext = fopen(PROJECT_DIR.'/'.LogUtil::$FolderName.'/'.LogUtil::$LogName.'.log', 'w');
                        fclose($newtext);
                    }
                }
                unset($file);
            }

            if(is_array($value)||is_object($value)){
                $saveString = json_encode($value);
            }else{
                $saveString = $value;
            }
            file_put_contents(PROJECT_DIR.'/'.LogUtil::$FolderName.'/'.LogUtil::$LogName.'.log', date('Y-m-d H:i:s',time())."\t".$saveString.PHP_EOL, FILE_APPEND );
        }catch(Exception $e){
            return false;
        }
        return true;
    }

    public static function printStackLog(){
        self::printLog(debug_backtrace());
    }

    public static function saveStackLog(){
        self::savelog(debug_backtrace());
    }
}