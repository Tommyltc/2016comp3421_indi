<?php

/**
 * Created by PhpStorm.
 * User: lindaltc
 * Date: 16/11/2016
 * Time: 7:45 PM
 */
class MathUtil
{
    const ATTR_MIN = "min";
    const ATTR_MAX = "max";
    /**
     * @param integer $length
     * @return array
     */
    public static function getIntegerRangeInLength($length){
        return array(self::ATTR_MIN=>1,self::ATTR_MAX=>pow(10,$length+1));
    }

    /**
     * @param integer|float $value
     * @param integer $length
     * @return bool
     */
    public static function validateInRange($value,$length){
        $range = self::getIntegerRangeInLength($length);
        return self::betweenOrEqual($value,$range[self::ATTR_MIN],$range[self::ATTR_MAX]);
    }

    /**
     * @param integer|float $value
     * @param integer|float $min
     * @param integer|float $max
     * @return bool
     */
    public static function betweenOrEqual($value,$min,$max){
        return $value>=$min&&$value<=$max;
    }
}