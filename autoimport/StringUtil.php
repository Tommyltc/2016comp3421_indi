<?php

/**
 * Created by PhpStorm.
 * User: lindaltc
 * Date: 16/11/2016
 * Time: 7:48 PM
 */
class StringUtil
{
    /**
     * @param string $str
     * @param integer $size
     * @param char $padChar
     * @return null|string
     */
    public static function lpad($str,$size,$padChar){
        if(sizeof($str)<=0) return null;
        $padLen = $size - sizeof($str);
        if($padLen<=0){
            return $str;
        }
        return self::padding($padLen,$padChar).$str;
    }

    /**
     * @param string $str
     * @param integer $size
     * @param char $padChar
     * @return null|string
     */
    public static function rpad($str,$size,$padChar){
        if(sizeof($str)<=0) return null;
        $padLen = $size - sizeof($str);
        if($padLen<=0){
            return $str;
        }
        return $str.self::padding($padLen,$padChar);
    }

    /**
     * @param integer $padLen
     * @param char $padChar
     * @return string
     */
    private static function padding($padLen,$padChar){
        $result = "";
        for($i=0;$i<$padLen;$i++){
            $result .= $padChar;
        }
        return $result;
    }

    /**
     * @param string $str
     * @param integer $length
     * @return bool
     */
    public static function validateLength($str,$length){
        return sizeof($str)<=$length;
    }
}