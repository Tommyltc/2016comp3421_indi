<?php
/**
 * Created by PhpStorm.
 * User: lindaltc
 * Date: 27/11/2016
 * Time: 11:36 PM
 */
$rStore = $_POST["rStore"];
$model = $_POST["model"];
$fname = $_POST["fname"];
$lname = $_POST["lname"];
$remail = $_POST["remail"];
$phone = $_POST["rphone"];
require_once "Configuration.php";

//CREATE SCHEMA `webindi` ;
//CREATE TABLE `webindi`.`reserve_record` (
//`ID` INT NOT NULL AUTO_INCREMENT,
//  `rStore` INT NOT NULL,
//  `model` VARCHAR(20) NOT NULL,
//  `fname` VARCHAR(20) NOT NULL,
//  `lname` VARCHAR(20) NOT NULL,
//  `remail` VARCHAR(100) NOT NULL,
//  `phone` VARCHAR(10) NOT NULL,
//  `create_time` TIMESTAMP(6) NOT NULL,
//  `status` VARCHAR(10) NULL,
//  PRIMARY KEY (`ID`));

//setting - START
const MAX_RESERVE_PER_DAY = 20;
const START_RESERVE_HOUR = 9;
const END_RESERVE_HOUR = 17;
//setting - END

$dateStart = DateTimeUtil::getDateStart(new DateTime());
$dateEnd = DateTimeUtil::getDateEnd(new DateTime());

$timeRangeMin = new DateTime();
$timeRangeMin->setTime(START_RESERVE_HOUR,0,0);
$timeRangeMax = new DateTime();
$timeRangeMax->setTime(END_RESERVE_HOUR,0,0);
$now = new DateTime();

if($timeRangeMin->getTimestamp()<$now->getTimestamp()&&$timeRangeMax->getTimestamp()>$now->getTimestamp()){
    global $dbAccess;
    $result = $dbAccess->query("SELECT * FROM reserve_record WHERE create_time >=FROM_UNIXTIME(".$dateStart->getTimestamp().") AND create_time <=FROM_UNIXTIME(".$dateEnd->getTimestamp().")");
    if(MAX_RESERVE_PER_DAY>sizeof($result)){
        $insert_sql = "INSERT INTO reserve_record (rStore,model,fname,lname,remail,phone,create_time) VALUES (".$rStore.",\"".$model."\",\"".$fname."\",\"".$lname."\",\"".$remail."\",".$phone.",FROM_UNIXTIME(".$now->getTimestamp()."))";
        $dbAccess->query($insert_sql);
        ob_end_clean();
        include "reserve_finish.html";
        die();
    }else{
        ob_end_clean();
        include "daily_reserve_period_ended.html";
        die();
    }

}else{
    ob_end_clean();
    include "daily_reserve_period_ended.html";
    die();
}

?>